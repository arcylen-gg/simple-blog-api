<?php

namespace App\Lib;
use Monolog\ErrorHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger as MonologLogger;

class Logger extends MonologLogger
{
    private static $logger = [];

    public function __construct($key = "app", $config = null)
    {
        parent::__construct($key);

        if (empty($config)) {
            $LOG_PATH = Config::get('LOG_PATH', __DIR__ . '/../../logs');
            $config = [
                'logFile' => "{$LOG_PATH}/{$key}.log",
                'logLevel' => MonologLogger::DEBUG
            ];
        }

        $this->pushHandler(new StreamHandler($config['logFile'], $config['logLevel']));
    }

    // Default APP log
    public static function getInstance($key = 'app', $config = null)
    {
        if (empty(self::$logger[$key])) {
            self::$logger[$key] = new Logger($key, $config);
        }

        return self::$logger[$key];
    }

    // Enable our error/request logs
    public static function enableSystemLogs()
    {
        $LOG_PATH = Config::get('LOG_PATH', __DIR__ . '/../../logs');

        // ERROR LOG
        self::$logger['error'] = new Logger('errors');
        self::$logger['error']->pushHandler(new StreamHandler("{$LOG_PATH}/errors.log"));
        ErrorHandler::register(self::$logger['error']);

        // REQUEST LOG
        $data = [
            $_SERVER,
            $_REQUEST,
            trim(file_get_contents('php://input'))
        ];
        // ERROR LOG
        self::$logger['request'] = new Logger('request');
        self::$logger['request']->pushHandler(new StreamHanlder("{$LOG_PATH}/request.log"));
        self::$logger['request']->info("REQUEST", $data);
    }

}